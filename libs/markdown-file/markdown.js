const fs = require('fs').promises
const yaml = require('js-yaml')
const remarkFrontmatter = require('remark-frontmatter')
const remarkHtml = require('remark-html')
const remarkParse = require('remark-parse')
const remarkStringify = require('remark-stringify')
const unified = require('unified')


class MarkdownFile {
  constructor(thisPath) {
    this.thisPath = thisPath

    this._read = this._read.bind(this)
    this._parseAst = this._parseAst.bind(this)
    this._getAst = this._getAst.bind(this)
    this._getFrontmatterAst = this._getFrontmatterAst.bind(this)
    this._getContentAst = this._getContentAst.bind(this)
    this.getFrontmatter = this.getFrontmatter.bind(this)
    this.getContentText = this.getContentText.bind(this)
    this.getContentHtml = this.getContentHtml.bind(this)
  }
  async _read() {
    const handle = await fs.open(this.thisPath)
    let content
    try {
      content = await handle.readFile()
    } finally {
      await handle.close()
    }
    return content.toString('utf8')
  }
  async _parseAst() {
    return unified()
      .use(remarkParse)
      .use(remarkFrontmatter)
      .parse(await this._read())
  }
  async _getAst() {
    if (!this._ast) {
      this._ast = await this._parseAst()
    }
    return this._ast
  }
  async _getFrontmatterAst() {
    const ast = await this._getAst()
    return ast.children.find(node => node.type === 'yaml')
  }
  async getFrontmatter() {
    const ast = await this._getFrontmatterAst()
    if (!ast) { return {} }
    return yaml.safeLoad(ast.value)
  }
  async _getContentAst() {
    const ast = await this._getAst()
    return {
      ...ast,
      children: ast.children.filter(node => node.type !== 'yaml')
    }
  }
  async getContentText() {
    const ast = await this._getContentAst()
    return unified()
      .use(remarkStringify)
      .stringify(ast)
  }
  async getContentHtml() {
    const ast = await this._getContentAst()
    return unified()
      .use(remarkHtml)
      .stringify(ast)
  }
}

module.exports = { MarkdownFile }
