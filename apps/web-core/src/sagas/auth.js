import { ApolloClient, InMemoryCache } from '@apollo/client';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import moment from 'moment-timezone';
import { all, call, delay, put, select, takeEvery } from 'redux-saga/effects';

import * as a from '../store/actions';
import * as s from '../store/selectors';

const { v4: uuid } = require('uuid');

const { httpErrorState } = a;

/* global MEMBER_PROFILES_URL */
/* global POLLS_URL */

const createBaseClient = () => axios.create({
  baseURL: MEMBER_PROFILES_URL
});
function createClient(token) {
  const client = createBaseClient();
  client.defaults.headers = { Authorization: `Bearer ${token}` };
  return client;
}
export const createPollsClient = (token, name) => {
  if (!localStorage.vicpahClientId)
    localStorage.vicpahClientId = uuid();
  const client = new ApolloClient({
    uri: POLLS_URL + '/graphql',
    cache: new InMemoryCache(),
    headers: {
      'x-vicpah-client-id': localStorage.vicpahClientId,
      ...(token ? { authorization: `Bearer ${token}` } : {}),
    },
  });
  client._name = name;
  return client;
}

const getTokenActionBody = (token, name) => {
  const payload = jwt.decode(token);
  const expiresIn = getExpInNs(payload);
  const noClient = _.isNumber(expiresIn) && expiresIn <= 0;
  const client = noClient ? null : createClient(token);
  const pollsClient = noClient ? null : createPollsClient(token, name);
  return { client, payload, pollsClient, token };
};


function* handleGetGuestToken({ recaptchaToken }) {
  const client = createClient(recaptchaToken);
  const resp = yield call(() => client.post('/guest_token', {}));
  yield* setToken({ tokenType: 'GUEST', token: resp.data.token });
}
function* handleGetLoginToken({ username, password }) {
  yield put({ type: 'LOGIN_STATE', state: 'pending' });
  try {
    const client = createBaseClient();
    const resp = yield call(() => client.post(
      '/login_token',
      {},
      { auth: { username, password } },
    ));
    yield* setToken({ tokenType: 'LOGIN', token: resp.data.token });
    yield put({ type: 'LOGIN_STATE', state: 'done' });
  } catch(err) {
    console.error(err);
    yield put(httpErrorState('LOGIN', err));
  }
}
function* handleGetRefreshToken() {
  const client = yield select(state => state.loginClient);
  try {
    const resp = yield call(() => client.post(
      '/refresh_token',
      {},
    ));
    yield put({ type: 'SET_REFRESH', ...getTokenActionBody(resp.data.token, 'refresh') });
  } catch (err) {
    console.error('Error getting refresh token', err)
    yield delay(5000);
    yield put({ type: 'GET_REFRESH_TOKEN' });
  }
}


function* setToken({ token, tokenType }) {
  yield put({ type: `SET_${tokenType}`, ...getTokenActionBody(token, tokenType) });
  if (tokenType === 'LOGIN')
    yield* handleGetRefreshToken()
}
function* clearClients({ tokenType }) {
  yield put({ type: `CLEAR_${tokenType}_CLIENTS` })
}


function* handlePersistToken({ type, token }) { // eslint-disable-line require-yield
  if (type === 'SET_GUEST') {
    localStorage.vicpahGuestToken = token;
  } else if (type === 'SET_LOGIN') {
    localStorage.vicpahLoginToken = token;
  } else {
    console.error('Unknown token type for persist:', type);
  }
}

function getExpInNs(payload) {
  if (_.isNil(payload.exp)) return null;
  const nowTs = moment.utc().valueOf();
  return Math.max(0, payload.exp * 1000 - nowTs);
}
function* delayExp(payload) {
  const ns = getExpInNs(payload)
  if (_.isNil(ns)) {
    return;
  } else if (ns > 0) {
    yield delay(ns);
  }
  console.log('expired', payload.sub);
}
function* handleGuestExpiry({ token, payload }) {
  yield* delayExp(payload);
  const currentToken = yield select(state => state.guestToken);
  if (token !== currentToken) return;
  yield clearClients({ tokenType: 'GUEST' });
}
function* handleRefreshExpiry({ token, payload }) {
  yield* delayExp(payload);
  const currentToken = yield select(state => state.refreshToken);
  if (token !== currentToken) return;
  yield* handleGetRefreshToken()
}

function* handleOauth({ service, code, client }) {
  yield put({ type: 'OAUTH_STATE', state: 'pending' });
  try {
    const fClient = client || createBaseClient();
    console.log('handleOauth fClient: %O', fClient)
    const resp = yield call(() => fClient.post(
      `/oauth2_${service}`,
      { code },
    ));
    if (resp.data.token) {
      yield* setToken({ tokenType: 'LOGIN', token: resp.data.token });
    } else {
      const loggedIn = yield select(s.getLoggedIn);
      yield put(a.getMember(loggedIn.id));
    }
    yield put({ type: 'OAUTH_STATE', state: 'done' });
  } catch(err) {
    console.error('Caught Oauth error', err);
    yield put(httpErrorState('OAUTH', err));
  }
}

export function* watchLogins() {
  const sagas = [
    takeEvery('GET_GUEST_TOKEN', handleGetGuestToken),
    takeEvery('GET_LOGIN_TOKEN', handleGetLoginToken),
    takeEvery('GET_REFRESH_TOKEN', handleGetRefreshToken),
    takeEvery('SET_LOGIN', handlePersistToken),
    takeEvery('SET_GUEST', handlePersistToken),
    takeEvery('SET_GUEST', handleGuestExpiry),
    takeEvery('SET_REFRESH', handleRefreshExpiry),
    takeEvery('OAUTH', handleOauth),
  ];

  if (localStorage.vicpahGuestToken)
    sagas.push(setToken({ tokenType: 'GUEST', token: localStorage.vicpahGuestToken }));
  if (localStorage.vicpahLoginToken)
    sagas.push(setToken({ tokenType: 'LOGIN', token: localStorage.vicpahLoginToken }));

  yield all(sagas);
}
