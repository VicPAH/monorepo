import createSagaMiddleware from 'redux-saga';
import { all, spawn, call } from 'redux-saga/effects';

import { watchLogins } from './auth';
import { watchMember } from './member';

export const sagaMiddleware = createSagaMiddleware();


function* rootSaga () {
  const sagas = [
    watchLogins,
    watchMember,
  ];

  yield all(sagas.map(
    saga => spawn(function* () {
      while (true) {
        try {
          yield call(saga)
          break
        } catch (err) {
          console.error(err)
        }
      }
    })
  ));
}

export function run() {
  sagaMiddleware.run(rootSaga);
}
