import React from 'react';
import rehypeRaw from 'rehype-raw';
import rehypeReact from 'rehype-react';
import remarkFrontmatter from 'remark-frontmatter';
import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import unified from 'unified';

import { MaybeLink } from '../components/MaybeLink';


export const getMarkdownParser = (parser=null) =>
  (parser ? parser : unified())
  .use(remarkParse)
  .use(remarkFrontmatter);

export const getMarkdownToReact = (parser=null) =>
  (parser ? parser : getMarkdownParser())
  .use(remarkRehype, { allowDangerousHtml: true })
  .use(rehypeRaw)
  .use(
    rehypeReact,
    {
      createElement: React.createElement,
      components: {
        a: ({ href, ...props }) => <MaybeLink to={ href } { ...props } />,
      },
    },
  );
