import * as ical from 'ical.js';
import _ from 'lodash';
import moment from 'moment-timezone';


const vcardToMoment = vcardTime => moment.tz(
  vcardTime.toString(),
  vcardTime.timezone || 'Australia/Melbourne',
);

export function timezoneToText(tz) {
  const parts = tz.split('/').map(_.startCase)
  parts.reverse()
  return parts.join(', ')
};


export class Calendar {
  static fromString(icalData) {
    return new Calendar(new ical.Component(ical.parse(icalData)));
  }

  constructor(icalComponent) {
    this.comp = icalComponent;
  }
  _getProps = () => ({
  })
  _getAllEvents = () => {
    return this.comp
      .getAllSubcomponents()
      .map(eventComponent => new Event(eventComponent))
  }
  getEvents = ({
    sortBy = 'dtstart',
  } = {}) => {
    return this._getAllEvents()
      .sort(Event.sortBy(sortBy))
  }
}

export class Event {
  static sortBy = field => (left, right) => {
    const { [field]: leftVal } = left._getProps()
    const { [field]: rightVal } = right._getProps()
    if (leftVal.isBefore(rightVal))
      return -1;
    if (leftVal.isAfter(rightVal))
      return 1;
    return 0;
  }
  constructor(icalComponent) {
    this.comp = icalComponent;
  }
  getUid = () => this.comp.getFirstPropertyValue('uid')
  _getProps = () => ({
    summary: this.comp.getFirstPropertyValue('summary'),
    url: this.comp.getFirstPropertyValue('url'),
    categories: this.comp.getFirstProperty('categories').getValues(),
    dtstart: vcardToMoment(this.comp.getFirstPropertyValue('dtstart')),
    dtend: vcardToMoment(this.comp.getFirstPropertyValue('dtend')),
    dtstartTz: this.comp.getFirstPropertyValue('dtstart').timezone,
    dtendTz: this.comp.getFirstPropertyValue('dtend').timezone,
  })
}
