import React, { PureComponent } from 'react';
import { Divider, Icon, Rail } from 'semantic-ui-react';
import styled from 'styled-components/macro';


const MetaRowWrapper = styled.div`
position: relative;
`
const TitleRowRail = styled(Rail)`
text-align: right;
`
const MetaRowRail = styled(TitleRowRail)`
font-size: 150% !important;
opacity: 65%;
`
const MetaRowLabel = styled.div`
font-weight: bold;
`
const createRowWithRail = WrappedRail => class RowWithRail extends PureComponent {
  render() {
    const { railContent, children } = this.props
    return (
      <MetaRowWrapper>
        <WrappedRail position="left">
          { railContent }
        </WrappedRail>
        { children }
      </MetaRowWrapper>
    )
  }
}
const MetaRowBase = createRowWithRail(MetaRowRail)
export class MetaRow extends PureComponent {
  render() {
    const { icon, label, noDivider, children } = this.props
    return (
      <MetaRowBase railContent={ <Icon name={ icon }/> }>
        { label && <MetaRowLabel>{ label }</MetaRowLabel> }
        { children }
        { !noDivider && <Divider /> }
      </MetaRowBase>
    )
  }
}
export const TitleRow = createRowWithRail(TitleRowRail)
