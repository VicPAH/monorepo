import _ from 'lodash';
import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { Button, Divider, Icon, Form, Label, Loader, Message, Modal } from 'semantic-ui-react';

import { Captcha } from './Captcha';

import * as a from '../store/actions';

/* global DISCORD_OAUTH_URL */


@connect(
  state => (_.pick(state, [
    'guestClient',
    'loginClient',
    'registerState',
    'registerErrors',
    'loginState',
    'loginErrors',
    'oauthState',
  ])),
  {
    getLoginToken: a.getLoginToken,
    register: a.register,
    oauth: a.oauth,
    setRegisterErrors: a.setRegisterErrors,
    setLoginErrors: a.setLoginErrors,
    setOauthErrors: a.setOauthErrors,
  },
)
class LoginModal extends PureComponent {
  state = {
    tab: 'login',
    dirty: {},
    values: {},
  }

  componentDidUpdate() {
    const { tab } = this.state;
    const { registerState } = this.props;
    if (tab === 'register' && registerState === 'success') {
      this.setState({ tab: 'login' });
    }
  }

  setTabLogin = () => {
    this.props.setLoginErrors([]);
    this.setState({ tab: 'login', values: {}, dirty: {} });
  }
  setTabRegister = () => {
    this.props.setRegisterErrors([]);
    this.setState({ tab: 'register', values: {}, dirty: {} });
  }

  handleChange = (ev, { name, value }) => {
    const{
      loginErrors, setLoginErrors,
      registerErrors, setRegisterErrors,
    } = this.props;
    const {
      dirty: currDirty,
      values: currValues,
      tab,
    } = this.state;
    const { errors: currErrors = [], setter: setErrors } = {
      login: { errors: loginErrors, setter: setLoginErrors },
      register: { errors: registerErrors, setter: setRegisterErrors },
    }[
      tab === 'register' ? 'register' : 'login'
    ];

    const errors = [ ...currErrors ];
    this.consumeErrors([name], errors);
    if (name === 'password') {
      this.consumeErrors(['passwordAgain'], errors);
    }

    const dirty = {
      ...currDirty,
      [name]: true,
    };
    const values = {
      ...currValues,
      [name]: value,
    };

    // Validate repeat password
    if (
      name === 'passwordAgain'
      || (name === 'password' && currDirty.passwordAgain)
    ) {
      if ((values.password || '') !== (values.passwordAgain || '')) {
        errors.push({
          path: ['passwordAgain'],
          message: 'Must match password',
        });
      }
    }

    this.setState({ dirty, values });
    if (!_.isEqual(currErrors, errors)) {
      setErrors(errors);
    }
  }
  handleSubmit = async () => {
    const { tab, values } = this.state;

    try {
      if (tab === 'register') {
        this.props.register(
          _.omit(values, ['passwordAgain']),
        );
      } else {
        this.props.getLoginToken(values.email, values.password);
      }
    } catch(err) {
      console.error(err);
    }
  }
  handleDiscordLogin = () => {
    document.location = DISCORD_OAUTH_URL;
  }

  consumeErrors = (path, errors) => {
    if (errors === this.props.registerErrors)
      throw new Error("Mustn't use props.registerErrors directly");

    return _
      .remove(errors, err => _.isEqual(err.path, path))
      .map(err => err.message);
  }
  consumeErrorsStr = (path, errors) => {
    const arr = this.consumeErrors(path, errors);
    return !!arr.length
      ? arr.join('\n')
      : null;
  }

  render() {
    const {
      loginClient,
      guestClient,
      loginState,
      loginErrors,
      registerState,
      registerErrors,
      oauthState,
      ...remainProps
    } = this.props;
    const { tab } = this.state;
    const errors = [ ...(
      tab === 'register'
        ? registerErrors
        : loginErrors
    ) || [] ];
    const loading = (
      loginState === 'pending' ||
      registerState === 'pending' ||
      oauthState === 'pending'
    );

    return (
      <Modal { ..._.pick(remainProps, ['open', 'onClose']) } closeIcon={ true } size="tiny" basic>
        <Modal.Header>{ _.startCase(tab) }</Modal.Header>
        <Modal.Content>
          <Form onSubmit={ this.handleSubmit } inverted error>
            <Form.Field
              control={ Form.Input }
              label="Email"
              name="email"
              key={ `${tab}Email` }
              placeholder="pupper@example.com"
              onChange={ this.handleChange }
              disabled={ loading }
              error={ this.consumeErrorsStr(['email'], errors) }
              required
            />
            { tab === 'register' && (
              <Label style={{ marginTop: '-5px', marginBottom: '5px' }}pointing>If you{"'"}re already a VicPAH financial member, use the same email that you submitted in your membership application. For help, email support@vicpah.org.au</Label>
            ) }
            <Form.Field
              control={ Form.Input }
              type="password"
              label="Password"
              name="password"
              key={ `${tab}Password` }
              onChange={ this.handleChange }
              disabled={ loading }
              error={ this.consumeErrorsStr(['password'], errors) }
              required
            />
            { tab === 'register' && (
              <Fragment>
                <Form.Field
                  control={ Form.Input }
                  type="password"
                  label="Password (Again)"
                  name="passwordAgain"
                  key={ `${tab}PasswordAgain` }
                  onChange={ this.handleChange }
                  disabled={ loading }
                  error={ this.consumeErrorsStr(['passwordAgain'], errors) }
                  required
                  />
                <Captcha />
              </Fragment>
            ) }
            { !!errors.length && (
              <Message
                content={ (
                  <Fragment>
                    { errors.map(err => <div key={ err.message }>{ err.message }</div>) }
                  </Fragment>
                ) }
                error
              />
            ) }
            {/* have to have a "fake" submit button for enter to work */}
            <button type="submit" style={{ display: 'none' }}/>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Fragment>
            <Button
              onClick={ tab === 'register' ? this.setTabLogin : this.setTabRegister }
              disabled={ loading }
              inverted
              basic
            >
              I { tab === 'register' ? 'already' : "don't" } have an account
            </Button>
            <Button
              onClick={ this.handleSubmit }
              loading={ loading && oauthState !== 'pending' }
              disabled={ loading || (tab === 'register' && !guestClient) }
              color="green"
              inverted
            >
              { tab === 'register' ? 'Register' : 'Login' }
            </Button>
            <Divider horizontal inverted>Or</Divider>
            <Button
              onClick={ this.handleDiscordLogin }
              color="violet"
              disabled={ loading }
              style={{ position: 'relative' }}
              fluid
            >
              { loading && oauthState === 'pending' && <Loader size="small" /> }
              <Icon name="discord" />
              { tab === 'register' ? 'Register' : 'Login' } with Discord
            </Button>
          </Fragment>
        </Modal.Actions>
      </Modal>
    );
  }
}

export { LoginModal };
