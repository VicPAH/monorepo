import _ from 'lodash';
import React, { useCallback } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { connect } from 'react-redux';

import * as a from '../store/actions';


/* global RECAPTCHA_KEY */


export const Captcha = connect(
  state => _.pick(state, ['guestClient', 'loginClient']),
  { getGuestToken: a.getGuestToken },
)(
  ({ getGuestToken, guestClient, loginClient }) => {
    const handleChange = useCallback(token => {
      getGuestToken(token);
    }, [getGuestToken]);
    if (guestClient || loginClient) return null;
    return (
      <ReCAPTCHA
        sitekey={ RECAPTCHA_KEY }
        theme="dark"
        onChange={ handleChange }
        />
    )
  },
);
