import React, { useEffect, useState } from 'react';

export const AdminRouteLoader = props => {
  console.log('AdminRouteLoader', props);
  const [AdminRoute, setAdminRoute] = useState(null);
  useEffect(() => {
    import(/* webpackChunkName: "admin" */ './lazy')
      .then(m => setAdminRoute(() => m.AdminRoute))
      .catch(err => console.error('Error loading admin routes', err));
  });
  return AdminRoute ? <AdminRoute {...props} /> : null;
};
