import React, { useEffect, useState } from 'react';
import { Button, Message, Modal } from 'semantic-ui-react';


const OVER_18 = Symbol('OVER_18');
const UNDER_18 = Symbol('UNDER_18');
const NEW_VISIT = Symbol('NEW_VISIT');

const parseStore = () => {
  if (!localStorage.vicpahOver18) {
    return NEW_VISIT;
  }
  return JSON.parse(localStorage.vicpahOver18)
    ? OVER_18
    : UNDER_18;
}


export const Over18Modal = props => {
  const [open, setOpen] = useState(parseStore());
  useEffect(() => {
    if (open === OVER_18)
      localStorage.vicpahOver18 = true;
    if (open === NEW_VISIT)
      delete localStorage.vicpahOver18;
  }, [open]);

  return (
    <Modal
      open={ open !== OVER_18 }
      dimmer="blurring"
      >
      <Modal.Header>You must be over 18</Modal.Header>
      <Modal.Content>
        <p>
          VicPAH is an incorporated not-for-profit association to help
          adults use pet play to aid in maintaining personal mental health.
        </p>
        <p>
          In order to protect adults and minors alike, you <strong>must be 18
          years of age</strong> in order to join our community.
        </p>
        { open === UNDER_18 && (
          <Message error>
            You have indicated that you are under 18.If this has occurred in
            error, contact support@vicpah.org.au for assistance.
          </Message>
        ) }
      </Modal.Content>
      { open === NEW_VISIT && (
        <Modal.Actions>
          <Button onClick={ () => setOpen(UNDER_18) } negative>I am under 18 years old</Button>
          <Button onClick={ () => setOpen(OVER_18) } positive>I am over 18 years old</Button>
        </Modal.Actions>
      ) }
    </Modal>
  );
}
