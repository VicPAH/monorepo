import React, { PureComponent } from 'react'


export const withTrailingSlash = WrappedComponent => class WithTrailingSlash extends PureComponent {
  componentDidMount() {
    this.ensureSlash()
  }
  componentDidUpdate() {
    this.ensureSlash()
  }
  ensureSlash = () => {
    if (!this.props.match.url.endsWith('/')) {
      this.props.history.replace(this.props.match.url + '/')
    }
  }
  render() {
    if (!this.props.match.url.endsWith('/')) {
      return null
    }
    return <WrappedComponent { ...this.props } />
  }
}
