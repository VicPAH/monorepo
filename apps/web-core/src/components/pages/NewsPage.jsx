import React, { PureComponent } from 'react';

import { withDirectoryList } from './ListPage';


@withDirectoryList
class NewsListPage extends PureComponent {
  render() {
    const { listData } = this.props
    if (listData) {
      return listData.items.map(i => <div key={ i.slug }>{ i.slug }</div>)
    }
    return null
  }
}

export { NewsListPage }
