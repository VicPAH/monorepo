import _ from 'lodash';
import React, { Fragment, PureComponent } from 'react';
import { Container, Icon, List } from 'semantic-ui-react';

import { createLoadPage, LoadPage } from './LoadPage';
import { ResponseContent } from './LoadPage/ResponseContent';
import { ResponseError } from './LoadPage/ResponseError';
import { MaybeLink } from '../MaybeLink';
import { TitleRow } from '../Rail';
import { Responsive } from '../Responsive';


export const primaryEventGroups = [
  'aphc',
  'hangout',
  'videohangouts',
  'ipahw',
  'midsumma',
  'mosh',
  'munch',
  'netflix',
  'netflixparty',
];


class EventDescriptionResponseError extends PureComponent {
  render() {
    const { group, response: { status } } = this.props
    if (status === 404) {
      return (
        <h1>
          { group === 'index'
            ? 'Events'
            : `${_.startCase(group)} events`
          }
        </h1>
      )
    }
    return <ResponseError { ..._.omit(this.props, ['group']) } />
  }
}
const EventDescriptionLoadPage = createLoadPage(ResponseContent, EventDescriptionResponseError)


export class EventGroupPageRoute extends PureComponent {
  componentDidMount() {
    this.redirect();
  }
  componentDidUpdate() {
    this.redirect();
  }
  redirect = () => {
    const { history, match: { url, params: { group } } } = this.props;
    if (url !== url.toLowerCase()) {
      history.push(url.toLowerCase());
      return;
    }
    if (primaryEventGroups.indexOf(group) >= 0) {
      if (url.startsWith('/events/')) {
        history.push(`/${group}/`);
        return;
      }
    }
    if (group === 'netflix') {
      history.push('/netflixparty/');
    }
    if (group === 'hangout') {
      history.push('/videohangouts/')
    }
  }
  render() {
    const { match: { params: { group } } } = this.props;
    return (
      <EventGroupPage
        group={ {
          netflixparty: 'netflix',
          videohangouts: 'hangout',
        }[group] || group }
        />
    );
  }
}


const RAIL_HIDE_WIDTH = 1100;


const NETFLIX_FAQS = [
  {
    q: 'Where do I need to be to join in?',
    a: 'This is an online event! That means you can watch anywhere you have the internet.',
  },
  {
    q: 'Can I join in at any time?',
    a: (
      <Fragment>
        <p>Yep! You don’t have to attend for the whole movie night, jump in starting at any movie, or even skip the ones you don’t want to watch.</p>
        <p>However, if you join part way through a movie, the only way you’ll be synced up perfectly is with the Netflix Party app. Feel free to manually seek through the movie to get to the right time.</p>
      </Fragment>
    )
  },
  {
    q: 'Do I need to use Discord?',
    a: (
      <Fragment>
        <p>If you want to be part of the group chat for the movies, then you will need a Discord account.</p>
        <p>Discord is available on Phones and Desktop.</p>
        <p>Just install the app, make an account, <a href="https://discord.gg/7Rer4uebAy">join our server</a>, and make your way to the # Netflix-Party channel and you’re ready to be part of the discussion. Awoo!</p>
      </Fragment>
    ),
  },
  {
    q: 'Do I need to use Netflix Party?',
    a: 'You do not need to use Netflix Party. Netflix Party is just a convenient way to sync up the start times of the movies. You can just start each movie at the start time and be synced up.',
  },
  {
    q: 'I can’t get Netflix Party to work',
    a: 'Netflix Party is only available for Desktop computers running Chrome/Chromium. If you can’t use it, just sync up the movie start times manually.',
  },
  {
    q: 'Is this an event with video / microphone chat?',
    a: 'There is no video chat at this event. There is an opt in voice chat channel on discord, but you can use the more popular standard text chat.',
  },
]

class NetflixPartyTemp extends PureComponent {
  state = {
    showDesktop: false,
    showDiscord: false,
    showVote: false,
  }

  createToggleFaq = key => () => {
    const fkey = `showFaq${key}`;
    this.setState({ [fkey]: !this.state[fkey]  });
  }
  getFaqShown = key => {
    const fkey = `showFaq${key}`;
    return this.state[fkey];
  }
  toggleDesktop = () => this.setState({ showDesktop: !this.state.showDesktop });
  toggleDiscord = () => this.setState({ showDiscord: !this.state.showDiscord });
  toggleVote = () => this.setState({ showVote: !this.state.showVote });

  renderToggle = shown => {
    return <Icon name={ `chevron ${shown ? 'down' : 'right'}` } style={{ float: 'left' }} />
  }

  render() {
    const { showDesktop, showDiscord, showVote } = this.state;

    return (
      <React.Fragment>
        <h1>Netflix Party</h1>
        <p>
          VicPAH Netflix party events allow us to share movies from our own
          kennels and couches. Join the chat on Discord, or just watch along
          with the rest of the PAH; it's up to you!
        </p>

        <p>
          These events are all online! This means you can watch from anywhere,
          and join in when it suits you, if you can’t make it to the whole
          event.
        </p>

        <p>
          Simply press play on your device at the start time of each movie. To
          make this easier, there's a countdown timer on each event page, to
          show you exactly when to push play. We also announce starting times
          on the discord channel, however since these are manual they might not
          be quite as precise.
        </p>

        <p>
          We also support the Netflix Party App, which works on any PC, or Mac
          with Chrome installed.
        </p>

        <List divided>
          { NETFLIX_FAQS.map(({ q, a }) => {
            const toggle = this.createToggleFaq(q);
            const shown = this.getFaqShown(q);
            return (
              <List.Item key={ q }>
                <List.Header onClick={ toggle }>{ q }{ this.renderToggle(shown) }</List.Header>
                { shown && <List.Description>{ a }</List.Description> }
              </List.Item>
            )
          } )}
        </List>

        <h2 onClick={ this.toggleVote }>Voting and adding movies to watch { this.renderToggle(showVote) }</h2>
        { showVote && (
          <p>
            We have a <MaybeLink to="/polls/dG9TrA2tG69GW4d2wEgp">list of movies</MaybeLink> that you can add to, and vote on.
          </p>
        ) }
        <h2 onClick={ this.toggleDesktop }>Using the Netflix Party App on Desktop { this.renderToggle(showDesktop) }</h2>
        { showDesktop && (
          <ol>
            <li>You'll need to be using <a href="https://www.google.com.au/chrome/">Chrome</a>, or Chromium on a device that allows extensions (Mac, Windows, Linux)</li>
            <li>Install the <a href="https://chrome.google.com/webstore/detail/netflix-party/oocalimimngaihdkbihfgmpkcpnmlaoa?hl=en">Netflix Party extension</a></li>
            <li>Sign in to <a href="https://netflix.com">Netflix</a> in the browser you installed the extension</li>
            <li>You're ready to go! Now you just have to wait for a watch link from either the movie event page, or the Discord channel</li>
            <li>You might want to setup your computer to display on your TV. We've <MaybeLink to="/netflix-party/tv-setup/">written some guides</MaybeLink> with helpful tips on how to do this.</li>
          </ol>
        ) }
        <h2 onClick={ this.toggleDiscord }>Using Discord to join in on the discussion { this.renderToggle(showDiscord) }</h2>
        { showDiscord && (
          <ol>
            <li>Join on time, or early! We will start the movie pretty close to when the event is scheduled to begin</li>
            <li>Join the Discord channel (#netflix-party when you've joined the server).</li>
            <li>There's a voice channel, and a text channel. The text channel will be most useful. If you choose to join the voice channel, make sure you set up your mic correctly (we have some rules)!</li>
            <li>Chat with other pets and handlers!</li>
            <li>We will pin a link in Discord, and update the event page a little while before we start the event</li>
          </ol>

        ) }
      </React.Fragment>
    );
  }
}

export class EventGroupPage extends PureComponent {
  renderCalendarLink = () => {
    const { group } = this.props;
    return (
      <a href={ `webcal://${document.location.host}/content/events/groups/${group}/index.ics` }>
        <Icon name="calendar plus outline" /> Calendar app
      </a>
    )
  }
  renderSubscribeRail = () => {
    return (
      <Responsive minWidth={ RAIL_HIDE_WIDTH }>
        Subscribe<br/>
        { this.renderCalendarLink() }
      </Responsive>
    )
  }
  renderSubscribeContent = () => {
    return (
      <Responsive maxWidth={ RAIL_HIDE_WIDTH + 1 }>
        Subscribe<br/>
        { this.renderCalendarLink() }
      </Responsive>
    )
  }
  render() {
    const { group } = this.props;
    return (
      <Container>
        <Container className="content" text>
          <TitleRow railContent={ this.renderSubscribeRail() }>
            { this.renderSubscribeContent() }
            { group === 'netflix'
                ? <NetflixPartyTemp / >
                : <EventDescriptionLoadPage
                    group={ group }
                    url={ `/content/events/groups/${group}/index.md` }
                    contentType="text/markdown"
                    noContainer
                    no404
                    />
            }
            <h2>Future events</h2>
            <LoadPage url={ `/content/events/groups/${group}/index.ics` } noContainer />
          </TitleRow>
        </Container>
      </Container>
    )
  }
}
