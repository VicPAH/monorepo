import { ApolloProvider } from '@apollo/client';
import React from 'react';
import ReactDOM from 'react-dom';
import { Helmet } from 'react-helmet';
import { connect, Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Router } from 'react-router-dom';
import { SemanticToastContainer } from 'react-semantic-toasts';
import * as Sentry from '@sentry/browser';
import { createGlobalStyle, ThemeProvider } from 'styled-components/macro';

import { App } from './components/App';
import { run as runSagas } from './sagas';
import { HISTORY, STORE } from './store';
import { pollsClientSel } from './store/selectors';
import { theme } from './theme';

// import 'typeface-overpass-mono';
import 'semantic-ui-css/semantic.min.css'
import 'react-semantic-toasts/styles/react-semantic-alert.css';
import 'react-square-payment-form/lib/default.css';

/* global SENTRY_DSN */
if (SENTRY_DSN) {
  Sentry.init({ dsn: SENTRY_DSN });
}


/* global SQUARE_DOMAIN */
const squareDomain = SQUARE_DOMAIN;


const GlobalStyle = createGlobalStyle`
#root {
  padding-bottom: 32px;
}
h1, h2, h3, h4, h5, h6 {
  color: ${props => props.theme.primary.toRgbString()} !important;
}
`

const WrappedApolloProvider = connect(pollsClientSel)(ApolloProvider);

const cspScriptSrc = [
  "'self'",

  // ReCAPTCHA
  'https://www.google.com',
  'https://www.gstatic.com',

  // Square payments
  `https://js.${squareDomain}`,
  'https://*.squarecdn.com',

  // Mapkit
  document.location.protocol === 'http:'
    ? 'cdn.apple-mapkit.com'
    : 'https://cdn.apple-mapkit.com',
  'blob:',

  // Commentbox
  'https://app.commentbox.io',
]

const MainSiteWrapper = props => (
  <ThemeProvider theme={ theme }>
    <Provider store={ STORE }>
      <WrappedApolloProvider>
        <Helmet>
          <meta
            httpEquiv="Content-Security-Policy"
            content={ `script-src ${cspScriptSrc.join(' ')}` }
            />
	  { Object.entries(theme.logoSizes).map(([size, url]) =>
	    <link rel="icon" href={ url } sizes={ `${size}x${size}` }/>
	  ) }
        </Helmet>
        <SemanticToastContainer />
        <GlobalStyle />
        <App />
      </WrappedApolloProvider>
    </Provider>
  </ThemeProvider>
);

const AppWrapper = props => (
  <Router history={ HISTORY }>
    <Switch>
      <Route
        path="/association/committee/"
        render={ () => {
          document.location = "https://www.mindtools.com/pages/article/fake-news.htm";
          return null;
        } }
        exact
        />
      <Route component={ MainSiteWrapper } />
    </Switch>
  </Router>
)


const render = () => {
  runSagas();
  const rootElement = document.getElementById("root");
  if (rootElement.hasChildNodes()) {
    ReactDOM.hydrate(<AppWrapper />, rootElement);
  } else {
    ReactDOM.render(<AppWrapper />, rootElement);
  }
};

render()
