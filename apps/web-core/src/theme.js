import color from 'tinycolor2';

/* eslint-disable no-unused-vars */
import logo from './img/logo.png';
import logo16 from './img/logo_16x.png';
import logo32 from './img/logo_32x.png';
import logo48 from './img/logo_48x.png';
import logo96 from './img/logo_96x.png';
import logo152 from './img/logo_152x.png';
import logo167 from './img/logo_167x.png';
import logo180 from './img/logo_180x.png';
import logo192 from './img/logo_192x.png';
import logoTrans from './img/logo-trans.png';
/* eslint-enable no-unused-vars */
import bgFull from './img/_MG_2247.jpg';
import bg2000 from './img/_MG_2247_2000x.jpg';
import bg1500 from './img/_MG_2247_1500x.jpg';
import bg1000 from './img/_MG_2247_1000x.jpg';
import bg500 from './img/_MG_2247_500x.jpg';


export const theme = {
  vicblue: color('#023b89'),
  qldcolor: color('#73182C'),
  nswcolor: color('#125069'),
  wacolor: color('#ccac00'),
  sacolor: color('#cc0000'),
  tascolor: color('#006a4e'),
  transblue: color('#55CDFC'),
  transpink: color('#F7A8B8'),
  spacer: 10,
  phoneWidth: 800,
  headerHeight: 500,
  headerSlope: 80,
  headerTopPad: 150,
  headerImage: {
    full: bgFull,
    2000: bg2000,
    1500: bg1500,
    1000: bg1000,
    500: bg500,
  },
  logo,
  logoSizes: {
    16: logo16,
    32: logo32,
    48: logo48,
    96: logo96,
    152: logo152,
    167: logo167,
    180: logo180,
    192: logo192,
    230: logo,
  },
};
const t = theme;
t.primary = t.vicblue;
t.primarylight = t.primary.clone().brighten(60);
t.primarydark = t.primary.clone().brighten(-30);
