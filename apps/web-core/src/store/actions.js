import _ from 'lodash';


export const getGuestToken = recaptchaToken => ({
  type: 'GET_GUEST_TOKEN', recaptchaToken,
});
export const getLoginToken = (username, password) => ({
  type: 'GET_LOGIN_TOKEN', username, password,
});


export const getMember = id => ({
  type: 'GET_MEMBER', id,
});
export const patchMember = data => ({
  type: 'PATCH_MEMBER', ...data,
});


export const submitApplication = id => ({
  type: 'SUBMIT_APPLICATION', id,
});
export const clearSubmitApplicationState = () => ({
  type: 'SUBMIT_APPLICATION_STATE',
  state: null, errors: null,
});
export const payMembership = data => ({
  type: 'PAY_MEMBERSHIP', ...data,
});
export const clearPayMembershipState = () => ({
  type: 'PAY_MEMBERSHIP_STATE',
  state: null, errors: null,
});


export const logout = () => ({ type: 'LOGOUT' });
export const register = data => ({
  type: 'REGISTER', ...data,
});
export const oauth = data => ({
  type: 'OAUTH', ...data,
});
export const setLoginErrors = errors => ({
  type: 'SET_LOGIN_ERRORS', errors,
});
export const setRegisterErrors = errors => ({
  type: 'SET_REGISTER_ERRORS', errors,
});
export const setOauthErrors = errors => ({
  type: 'SET_OAUTH_ERRORS', errors,
});


export const httpErrorState = (prefix, err, meta={}) => ({
  ...meta,
  type: `${prefix}_STATE`,
  state: 'error',
  errors: err.isAxiosError && _.has(err, 'response.data.errors')
    ? err.response.data.errors
    : [{ message: 'Unknown error', path: [] }],
});
