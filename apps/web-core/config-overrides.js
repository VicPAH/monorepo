const {
  addBabelPlugin,
  addWebpackPlugin,
  override,
} = require('customize-cra');
const _ = require('lodash');
const process = require('process');
const webpack = require('webpack');


module.exports = {
  webpack: override(
    addBabelPlugin(
      ['@babel/plugin-proposal-decorators', { legacy: true }],
    ),
    addWebpackPlugin(new webpack.DefinePlugin(_.mapValues(
      {
        ENV: process.env.ENV || 'dev',
        SENTRY_DSN: process.env.SENTRY_DSN
          || '',
        MAPKIT_JWT_URL: process.env.APPLE_MAPS_URL
          || 'https://australia-southeast1-apple-maps-30d1-dev.cloudfunctions.net',
        PHOTO_COMP_2020_API_URL: process.env.PHOTO_COMP_2020_API_URL
          || 'https://9hq1gfabt8.execute-api.ap-southeast-2.amazonaws.com/dev',
        PHOTO_COMP_2020_STATIC_URL: process.env.PHOTO_COMP_2020_STATIC_URL
          || 'photo-comp-dev-photo-output-lwqhetubhoehtqwovjk.s3.amazonaws.com',
        MEMBER_PROFILES_URL: process.env.MEMBER_PROFILES_URL
          || 'http://localhost:8080',
        POLLS_URL: process.env.POLLS_URL
          || 'http://localhost:8081',
        RECAPTCHA_KEY: process.env.RECAPTCHA_KEY
          || '6Lf9CcIZAAAAAE68g2Dm4Qdi_KHSZhzcnVB7SUJY',
        SQUARE_APPLICATION_ID: process.env.SQUARE_APPLICATION_ID,
        SQUARE_LOCATION_ID: process.env.SQUARE_LOCATION_ID,
        SQUARE_DOMAIN: process.env.SQUARE_DOMAIN
          || 'squareupsandbox.com',
        SQUARE_SANDBOX: !!process.env.SQUARE_SANDBOX,
        DISCORD_OAUTH_URL: process.env.DISCORD_OAUTH_URL
          || 'https://discord.com/api/oauth2/authorize?client_id=893667166442913794&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth%2Fdiscord%2Fcallback&response_type=code&scope=identify%20email',
        DISCORD_ADMIN_OAUTH_URL: process.env.DISCORD_ADMIN_OAUTH_URL
          || 'https://discord.com/api/oauth2/authorize?client_id=893667166442913794&permissions=395607010368&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth%2Fdiscord%2Fcallback&response_type=code&scope=identify%20email%20bot',
        ICAL_HOST_REPLACE_DEF: (
          process.env.ICAL_HOST_REPLACE_DEF
            || 'vicpah.org.au,www.vicpah.org.au,vicpah.com.au,www.vicpah.com.au,vicpah.gitlab.io'
        ).split(','),
      },
      v => JSON.stringify(v),
    ))),
  ),
}
