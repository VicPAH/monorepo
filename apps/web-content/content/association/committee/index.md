---
title: Committee members
layout: committee
members:
- name: Schism
  role: President
  prefix: He/Him
  email: president@vicpah.org.au
- name: Wolf
  role: Vice-President
  prefix: He/Him
  email: vicepresident@vicpah.org.au
- name: Scuba
  role: Secretary
  prefix: He/Him
  email: secretary@vicpah.org.au
- name: Tropes
  role: Treasurer
  prefix: He/Him
  email: treasurer@vicpah.org.au
- name: Biru
  role: Technology Support
  prefix: He/Him
  email: support@vicpah.org.au
- name: Kitsune Anubis
  role: General member
  prefix: He/Him
- name: Morty
  role: General member
  prefix: He/Him
---
# VicPAH committee members
Committee members meet at least once monthly to discuss relevant business relating to the
running of VicPAH, as well as ensuring that all VicPAH-run events run smoothly.
