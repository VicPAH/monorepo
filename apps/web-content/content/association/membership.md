# Membership
VicPAH holds many events throughout the year, and membership gets you a front row seat. Members receive a number of benefits from discount entry to many VicPAH events, to special pricing at a range of incredible Melbourne businesses that support our community.

* The cost to become a VicPAH member for 2021 is **$35** (or $20 concession)
* Membership runs for a calendar year from 1st January to 31st December
* Members will recieve an exclusive 2021 membership token, proving their membership in the first year of VicPAH's incorporation (details are being finalised; tokens will not be available immediately)
* Members must have a current residential address in the state of Victoria, Australia
* Members must provide the details of a single individual who will be receiving membership (real details; not scene name)
* Members may vote at general meetings, and may have other rights as determined by the committee or by resolution at a general meeting
* Members must agree to support the purposes of the association, and abide by [the rules](/association/constitution/)
* You can apply to become a member at many VicPAH events
* Applications are subject to committee approval

## Becoming a member online
Our membership system allows new members to apply and pay for membership, and
allows current members to manage their details and pay their annual membership
fees.

1. Sign up for an account by clicking the "Login" button at the top right of the site, then clicking "I don't have an account"
2. Enter an email address and password
3. Go to your profile page by clicking your email in the top right of the site, and click "Profile"
4. You should receive an email with a code in it. Enter the code into the text box on your profile page to verify that your email address is correct
5. Fill in all the required fields, and follow the instructions to apply and pay for membership
6. The VicPAH committee needs to approve all memberships. Your profile page will update to say that your membership is current when this occurs
7. When approved, you can use your profile page to update any of your contact details, see the status of your membership, and pay your membership fees when they are due

## Maintaining your existing membership online
1. Sign up for an account by clicking the "Login" button at the top right of the site, then clicking "I don't have an account"
2. Enter an email address and password<br/>
   Use the same email that you submitted in your membership application so that we can associate your existing membership details with your new account. For help, email support@vicpah.org.au
3. Go to your profile page by clicking your email or name in the top right of the site, and click "Profile"
4. You should receive an email with a code in it. Enter the code into the text box on your profile page to verify that you own the email address associated with your membership details
5. You can now update any of your contact details, see the status of your membership, and pay your membership fees when they are due
