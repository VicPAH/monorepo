# Connecting up your TV (optional)
## General notes
### WiFi display sharing (AirPlay, Windows Connect, Miracast)
These are unlikely to work perfectly, but are reasonable options. There are some
things that you can do to improve your experience:

- Make sure you're connected to a 5.0 GHz WiFi (you probably are); 2.4 GHz
  networks will be a bit choppier
  - On MacOS you can check by holding option and clicking the WiFi button in
    the toolbar. Under your network, it should show "Channel: ... 5 GHz")<br/>
    ![2.4 GHz Network](/assets/netflix-party/mac-wifi-2.4.png)
    ![5.0 GHz Network](/assets/netflix-party/mac-wifi-5.0.png)
- Shut down things running in the background

## MacOS
### HDMI
This is probably going to be the best bet for easy, smooth video. It's
[pretty straight forward](https://support.apple.com/en-us/HT204388) if your Mac
has an HDMI port, though newer MacBooks only have USB-C. You can buy an adapter
in this case.

### AirPlay to an Apple TV
AirPlay does a reasonable job with streaming videos to the TV.

1. Click the AirPlay menu, and click the Apple TV that you want to stream to<br/>
   ![AirPlay Menu](/assets/netflix-party/airplay-menu.png)
1. Make sure that the menu says "Mirror <name of your Apple TV>" rather than
   "Mirror Built-In Retina Display" or similar to get the best fit<br/>
   ![AirPlay Mirror to Apple TV](/assets/netflix-party/airplay-mirror-atv.png)
1. If you're using a MacBook you must keep the screen open. You can turn your
   screen off by [turning the brightness down](https://support.apple.com/en-au/guide/mac-help/mchlp2704/mac)
   to 0. You can also [turn off the keyboard backlight](https://support.apple.com/en-au/guide/mac-help/mchlp2265/mac)

## Windows
There are several methods to get Windows connected to a TV. Rather than writing
yet another guide, we've chosen
[a writeup by another site](https://www.windowstechit.com/20079/connect-pc-to-tv-windows-10/).

## Linux
You're on your own, but we all know you like to work things out for yourself.
