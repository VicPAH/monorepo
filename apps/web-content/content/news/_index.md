---
outputs:
- HTML
- JSON
---
# News
Updates about new and changed VicPAH events, membership updates, and anything
else that VicPAH members might like to know.
