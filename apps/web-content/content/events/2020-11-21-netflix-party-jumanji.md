---
title: "Netflix Party - Jumanji"
timezone: australia/melbourne
start: 2020-11-21T21:00:00
end: 2020-11-21T22:44:00
eventGroups: [vicpah, netflix]
movieId: 10270
synopsisText: "When siblings Judy and Peter discover an enchanted board game that opens the door to a magical world, they unwittingly invite Alan -- an adult who's been trapped inside the game for 26 years -- into their living room. Alan's only hope for freedom is to finish the game, which proves risky as all three find themselves running from giant rhinoceroses, evil monkeys and other terrifying creatures."
partyLink: https://www.tele.pe/netflix/51b96f7bf58b9d95?s=s143
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our final movie for the day will be Jumanji

## Party link
[Click here to join](https://www.tele.pe/netflix/51b96f7bf58b9d95?s=s143)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
When siblings Judy and Peter discover an enchanted board game that opens the door to a magical world, they unwittingly invite Alan -- an adult who's been trapped inside the game for 26 years -- into their living room. Alan's only hope for freedom is to finish the game, which proves risky as all three find themselves running from giant rhinoceroses, evil monkeys and other terrifying creatures.
