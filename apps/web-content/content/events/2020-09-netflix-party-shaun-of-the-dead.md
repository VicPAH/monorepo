---
title: "Netflix Party - Shaun of the Dead"
timezone: australia/melbourne
start: 2020-09-26T19:00:00
end: 2020-09-26T20:39:00
eventGroups: [vicpah, netflix]
movieId: 88828
synopsisText: "Shaun lives a supremely uneventful life, which revolves around his girlfriend, his mother, and, above all, his local pub. This gentle routine is threatened when the dead return to life and make strenuous attempts to snack on ordinary Londoners."
partyLink: https://www.netflix.com/watch/70003227?npSessionId=37781948e5ce1038&npServerId=s82
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be Shaun of the Dead

## Party link
[Click here to join](https://www.netflix.com/watch/70003227?npSessionId=37781948e5ce1038&npServerId=s82)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Shaun lives a supremely uneventful life, which revolves around his girlfriend, his mother, and, above all, his local pub. This gentle routine is threatened when the dead return to life and make strenuous attempts to snack on ordinary Londoners.
