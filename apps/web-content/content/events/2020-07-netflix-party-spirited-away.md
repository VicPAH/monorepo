---
title: Netflix Party - Spirited Away
timezone: australia/melbourne
start: 2020-07-18T21:15:00
end: 2020-07-18T23:20:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our final movie for the day will be Spirited Away.

## Party link
[Click here to join](https://www.netflix.com/watch/60023642?npSessionId=8049fcfa57ff0a8b&npServerId=s23)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
During her family's move to the suburbs, a sullen 10-year-old girl wanders into a world ruled by gods, witches, and spirits, and where humans are changed into beasts.
