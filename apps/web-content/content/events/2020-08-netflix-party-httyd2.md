---
title: "Netflix Party - How to Train Your Dragon: The Hidden World"
timezone: australia/melbourne
start: 2020-08-22T19:45:00
end: 2020-08-22T21:29:00
eventGroups: [vicpah, netflix]
movieId: undefined
synopsisText: "After meeting an enchanted creature, Hiccup and Toothless set out to find a legendary dragon paradise before evil hunter Grimmel finds them first."
partyLink: https://www.netflix.com/watch/81021806?npSessionId=ddf856f8ff6d3958&npServerId=s41
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be How to Train Your Dragon: The Hidden World

## Party link
[Click here to join](https://www.netflix.com/watch/81021806?npSessionId=ddf856f8ff6d3958&npServerId=s41)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
After meeting an enchanted creature, Hiccup and Toothless set out to find a legendary dragon paradise before evil hunter Grimmel finds them first.
