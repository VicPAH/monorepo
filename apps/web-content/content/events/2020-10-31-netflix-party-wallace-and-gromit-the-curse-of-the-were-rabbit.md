---
title: "Netflix Party - Wallace & Gromit: The Curse of the Were-Rabbit"
timezone: australia/melbourne
start: 2020-10-31T19:00:00
end: 2020-10-31T20:25:00
eventGroups: [vicpah, netflix]
movieId: 160757
synopsisText: "Cheese-loving eccentric Wallace and his cunning canine pal, Gromit, investigate a mystery in Nick Park's animated adventure, in which the lovable inventor and his intrepid pup run a business ridding the town of garden pests. Using only humane methods that turn their home into a halfway house for evicted vermin, the pair stumble upon a mystery involving a voracious vegetarian monster that threatens to ruin the annual veggie-growing contest."
partyLink: https://www.tele.pe/netflix/a646647e39d02e62?s=s156
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be Wallace & Gromit: The Curse of the Were-Rabbit

## Party link
[Click here to join](https://www.tele.pe/netflix/a646647e39d02e62?s=s156)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Cheese-loving eccentric Wallace and his cunning canine pal, Gromit, investigate a mystery in Nick Park's animated adventure, in which the lovable inventor and his intrepid pup run a business ridding the town of garden pests. Using only humane methods that turn their home into a halfway house for evicted vermin, the pair stumble upon a mystery involving a voracious vegetarian monster that threatens to ruin the annual veggie-growing contest.
