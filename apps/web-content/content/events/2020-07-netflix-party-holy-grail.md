---
title: Netflix Party - Monty Python and the Holy Grail
timezone: australia/melbourne
start: 2020-07-18T19:30:00
end: 2020-07-18T21:02:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be Monty Python and the Holy Grail

## Party link
[Click here to join](https://www.netflix.com/watch/771476?npSessionId=3c691561344bdfe0&npServerId=s69)

Remember to click the Netflix Party extension icon to join the party!
## Synopsis
King Arthur and his Knights of the Round Table embark on a surreal, low-budget search for the Holy Grail, encountering many, very silly obstacles.
