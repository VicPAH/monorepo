---
title: APHC opening event
timezone: australia/adelaide
start: 2019-11-14T20:30:00
end: 2019-11-14T23:00:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/opening-night-of-competitions
adr:
  name: The Wakefield Hotel
  street_address: 76 Wakefield Street
  suburb: Adelaide
  state: South Australia
---
Opening night of the Mr & Ms Adelaide Leather 2019 and Australian Pup & Handler
competitions. Come meet the contestants competing this year.
