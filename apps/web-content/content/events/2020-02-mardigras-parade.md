---
title: Mardi Gras Parade
timezone: Australia/Sydney
start: 2020-02-29T19:00:00
end: 2020-02-29T22:00:00
eventGroups: [interstate,major]
adr:
  street_address: Oxford St
  suburb: Sydney
  state: NSW
---
Building on four decades of powerful protest and LGBTQI celebration, the annual Sydney Gay and Lesbian Mardi Gras Parade is the biggest LGBTQI night of the year. A bustling extravaganza that brings Sydney to a standstill and shines a global spotlight on LGBTQI lives, culture, communities and creativity. With spectacular floats, thousands of participants, a veritable smorgasbord of glittering colour and dynamic shape, the parade showcases the glitz and glamour of our community.

Drag Queens, Dykes on Bikes, Sydney Lifesavers, community heroes and much much more - all uniting to produce what has become the parade that stops the nation. Expect colour, expect life, expect huge crowds of excited parade viewers lining the streets. Make your way to Oxford Street or Flinders Street early to secure a good spot!
