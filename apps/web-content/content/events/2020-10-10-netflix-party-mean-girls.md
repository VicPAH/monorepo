---
title: "Netflix Party - Mean Girls"
timezone: australia/melbourne
start: 2020-10-10T19:00:00
end: 2020-10-10T20:37:00
eventGroups: [vicpah, netflix]
movieId: 91952
synopsisText: "Cady Heron is a hit with The Plastics, the A-list girl clique at her new school, until she makes the mistake of falling for Aaron Samuels, the ex-boyfriend of alpha Plastic Regina George."
partyLink: https://www.tele.pe/netflix/cf8c9c65b2fe48ce?s=s143
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

We will only be watching 1 movie for tonight: Mean Girls

## Party link
[Click here to join](https://www.tele.pe/netflix/cf8c9c65b2fe48ce?s=s143)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Cady Heron is a hit with The Plastics, the A-list girl clique at her new school, until she makes the mistake of falling for Aaron Samuels, the ex-boyfriend of alpha Plastic Regina George.
