---
title: VicPAH 6th Birthday Party
timezone: australia/melbourne
start: 2021-04-16T19:30:00
end: 2021-04-16T23:00:00
eventGroups: [vicpah]
cost: |-
  Entry tickets are $10, and ticket holders will receive a $10 voucher for either drink/food.

  Check out [dtshotel.com.au](https://www.dtshotel.com.au/) to see the prices for pizza.
dressCode: |-
  We can wear any gear we like as long as there is no nudity, including explicit crotch bulges under clothing. Otherwise wear leather, rubber, neoprene, drag, anything you like!

  The venue will still be open to the general public, so make sure you are comfortable being seem by muggles in your outfit.
adr:
  name: DT's Hotel
  street_address: 164 Church St
  suburb: Richmond
  state: VIC
tryBookingEid: 739445
---
VicPAH is celebrating their 6th Birthday at DT's Hotel!<br/>
The event will start with pizza at 7:30, and then include a rope demo, cake, and more!

Our meets are open to all diverse gender, and sexual identities. Whether you're an experienced Pup or Handler, or simply curious to see if it's something that may interest you, we have a welcoming and safe space for you to enjoy.

VicPAH and DT's are both taking covid safety seriously, as such this event will be following COVID-Safe guidelines.
Check-in's through the Services Victoria app will be required for entry.

If you have any questions, concerns, or if you're feeling nervous, please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.
