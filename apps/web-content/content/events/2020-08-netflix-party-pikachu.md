---
title: Netflix Party - Detective Pikachu
timezone: australia/melbourne
start: 2020-08-01T17:00:00
end: 2020-08-01T18:44:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our first movie for the day will be Detective Pikachu.

## Party link
[Click here to join](https://www.netflix.com/watch/81037695?npSessionId=90c1410a4cacdad9&npServerId=s109)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
In a world where humans and Pokémon coexist, an electrifying supersleuth teams with his missing partner's son to crack the case of his disappearance.
