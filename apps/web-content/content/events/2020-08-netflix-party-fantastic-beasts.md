---
title: Netflix Party - Fantastic Beasts and Where to Find Them
timezone: australia/melbourne
start: 2020-08-22T17:00:00
end: 2020-08-22T19:12:00
eventGroups: [vicpah, netflix]
movieId: undefined
synopsisText: Long before Harry Potter came to Hogwarts, an adventurous "magizoologist" paid the Big Apple a visit.
partyLink: https://www.netflix.com/watch/80111501?npSessionId=0701280c9cff84d0&npServerId=s41
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be Netflix Party - Fantastic Beasts and Where to Find Them

## Party link
[Click here to join](https://www.netflix.com/watch/80111501?npSessionId=0701280c9cff84d0&npServerId=s41)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Long before Harry Potter came to Hogwarts, an adventurous "magizoologist" paid the Big Apple a visit.
