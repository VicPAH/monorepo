---
title: Gear'd market hall
timezone: australia/adelaide
start: 2019-11-16T12:00:00
end: 2019-11-16T17:00:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/geard-market-hall
adr:
  name: The Wakefield Hotel
  street_address: 76 Wakefield Street
  suburb: Adelaide
  state: South Australia
---
Fetish providers show their wares for your perusal and purchase along side some
of the country’s leading fetish educators sharing their craft and skills.
