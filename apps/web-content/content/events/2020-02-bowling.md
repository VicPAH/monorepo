---
title: Bowling
timezone: australia/melbourne
start: 2020-02-24T18:00:00
end: 2020-02-24T20:00:00
eventGroups: [vicpah, victoria]
cost: $19 per person to play, but bowling itself is optional
adr:
  name: Strike Bowling QV
  street_address: 245-247 Little Lonsdale Street
  suburb: Melbourne
---
End your Monday with puppies! Come and have a game or 2, some social drinks, or just socialize.
