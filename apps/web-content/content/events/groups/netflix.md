# Netflix Party
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

## Before the event [+]
1. You'll need to be using [Chrome](https://www.google.com.au/chrome/), or
   Chromium on a device that allows extensions (Mac, Windows, Linux)
1. Install the [Netflix Party extension](https://chrome.google.com/webstore/detail/netflix-party/oocalimimngaihdkbihfgmpkcpnmlaoa?hl=en)
1. Sign in to [Netflix](https://netflix.com) in the browser you installed the
   extension
1. You're ready to go! Now you just have to wait for a watch link from the PAH
   committee
1. You might want to setup your computer to display on your TV. We've
   [written some guides](/netflix-party/tv-setup/) with helpful tips on how to
   do this.

### Alternatively
You don't have to use Netflix Party. We will start the party at the exact time
that we have put on the event, so you can simple press play on your device at
the same time. To aid this, there's a countdown timer on each event page. It
will show a countdown, with seconds 5min before the event is about to begin.

We will also announce starting times on the discord channel, however since these
are manual they might not be quite as precise.

## When we're about to begin
1. Join on time, or early! We will start the movie pretty close to when the
   event is scheduled to begin
1. Join the [Discord](https://discord.gg/q9ExhAh) channel (#netflix-party when
   you've joined the server). You don't have to do this, but it's the easiest
   way to follow what's happening
   - There's a voice channel, and a text channel. The text channel will be most
     useful. If you choose to join the voice channel, make sure you
     [set up your mic correctly](/netflix-party/discord-voice/) (we have some
     rules)!
1. Chat with other pets and handlers!
1. We will pin a link in Discord, and update the event page a little while
   before we start the event
1. Click the link to open the Netflix video
1. Click the Netflix party extension icon to join the party<br/>
   ![Netflix party join](/assets/netflix-party/disable-chat.png)
   - We suggest disabling the Netflix party chat while you're here: We'll all
     be in Discord!
