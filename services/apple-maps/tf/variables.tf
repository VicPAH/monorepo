variable "region" {
  default = "australia-southeast1"
}
variable "commit_short_sha" {
  type = string
}
variable "google_project_id_prefix" {
  type = string
}


variable "apple_maps_key_b64" {
  type = string
}
