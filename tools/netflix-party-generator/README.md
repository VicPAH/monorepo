# Netflix Party Automation

This is a script to automate creation, and running of the Netflix Party events.

## new

Creates a new Netflix Party on the next 4th Saturday, with 3 movies.

```
usage: netflixParty new -i MOVIE_1 -i MOVIE_2 -i MOVIE_3

  --movie-id | -i   JustWatch movie ID to add
```

## link

Updates an event with a Netflix Party share link, and sends a message to Discord.

```
usage: netflixParty link --idx IDX --link LINK

  --idx  | -i   Index from 1 (first) to 3 (final) of the movie
  --link | -l   Link to update with
```

## countdown

Sends countdowns for 60, 30, 3, 2, 1, and 0 seconds to go, to Discord.

```
usage: netflixParty countdown --idx IDX

  --idx  | -i   Index from 1 (first) to 3 (final) of the movie
```

## secrets.json

Contains secrets that won't be committed to git

```json
{
  "discord_webhook": "webhook URL from Discord"
}
```
